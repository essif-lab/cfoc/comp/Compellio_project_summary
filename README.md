Product Circularity Data Sheets Digital Passport (PCDS-DP) by Compellio - Project Summary
===================

The Product Circularity Data Sheets Digital Passport (PCDS-DP) is a trusted accountability system delivering decentralized verification to businesses and auditors in circular supply chains.

With PCDS-DP, Compellio enhances trust and accountability within the PCDS ecosystem by building blockchain-enabled verifiable data and verifiable credentials that leverage SSI components to ensure trusted authentication, auditability, and data access services in Europe and beyond. 

Driven by the Luxembourg Ministry of the Economy and supported by major international industry leaders, the PCDS initiative addresses the difficulty for industry and consumers to access reliable data on the circular properties of a product. For each product, an internationally accepted dataset will describe all relevant information in controlled and auditable statements, helping the consumer and manufacturer to make educated choices, increasing the value of the product and enabling future uses in a circular economy. Since its inception, more than 50 companies from 12 different European countries, including global industry leaders, have joined the initiative.

Website: https://compell.io 

Contact: hello@compell.io 


